# GPSI Device Barcode Scanner 

```bash
iOS rejected the Validation app from the app store.  This happened due to the deprecation of the mobile framework used to create the app.
This POC application is a React webmobile barcode scanner application to replace the validation app.
```
# Demo

```bash
https://beta.gpsinsight.com/d/gpsi_validate_device/
```
# Clone the Repository

```bash
git clone git@gitlab.com:criss_moosman/poc-validation-app.git
```

# Installation

```bash
npm install
```

# Development

```bash
npm run dev
```

