/* eslint-disable no-console */
import React, { useState, useEffect } from 'react';
import Quagga from 'quagga';

import { withRouter } from 'react-router';

import BarcodeInputField from '../uiElements/barcodeInputField';

import VideoSkeleton from './Video.skeleton';

import './video.css';

const Video = (props) => {
  const {
    handleLoadScanner
  } = props;
  const [ videoInit, setVideoInit ] = useState(false);
  const [ videoError, setVideoError ] = useState(false);
  const [ attempts, setAttempts ] = useState(0);

  const onInitSuccess = () => {
    Quagga.start();
    setVideoInit(true);
  }

  const onInfoFetched = (res) => {
    if(res) {
      const { code } = res.codeResult;
      handleLoadScanner(code);
      Quagga.stop();
    } else {
      setAttempts(prevState => prevState + 1);
    }
  }

  const onDetected = (result) => {
    Quagga.offDetected(onDetected);
    onInfoFetched(result);
  }

  useEffect(() => {
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      Quagga.init({
        inputStream : {
          name : "Live",
          type : "LiveStream",
          target: document.querySelector('#video')
        },
        numOfWorkers: 1,
        locate: true,
        decoder : {
          readers : ['ean_reader', 'ean_8_reader', 'upc_reader', 'code_128_reader']
        }
      }, (err) => {
          if (err) {
            setVideoError(true);
            return;
          }
          onInitSuccess();
      });
      Quagga.onDetected(onDetected);
    }
  }, []);

  const onDeviceNotFound = () => {
    console.log('not-found');
  }

  useEffect(() => {
    if (attempts > 3) {
      onDeviceNotFound();
    }
  }, [attempts]);

  return (
    <div>
      <div className="video__explanation">
        <p>Scan a device barcode</p>
      </div>
      <div className="video__container">
        {videoError ?
          <div className="skeleton__unsopported">
            <div>
              <p>Your device does not support camera access or something went wrong <span role="img" aria-label="thinking-face">🤔</span></p>
              <p>You can enter the barcode below</p>
              <BarcodeInputField />
            </div>
          </div>
          :
          <div>
            <div className="video" id="video" />
            {videoInit ? '' : <VideoSkeleton />}
          </div>
        }
      </div>
    </div>
    );
}

export default withRouter(Video);
