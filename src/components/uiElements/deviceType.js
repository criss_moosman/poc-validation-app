import React from 'react';
import { Row, Col, Select } from 'antd';

const DeviceTypes = (props) => {
    const { Option } = Select;
    const deviceList = [
        "Select Device Type",
        "st1100",
        "tt1800",
        "tt3000",
        "gpsi4000_vpod",
        "gpsi4000_hardwire", 
        "at3000_motion",
        "pnp",
        "gpsi3900_jpod",
        "gpsi3900_vpod",
        "gpsi3900_hardwire",
        "gpsi4000hd", 
        "GPSI-4100HDAB"
    ];

    const [devices, setDevices] = React.useState(deviceList[0]);

    const handleDeviceChange = value => {
        setDevices(devices[value]);
        props.handleDeviceTypeSwitch(value);
        props.enableNextButton();
    };

    return(
        <div className="">
            <Row>
                <Col span={6}>
                    <Select id="device-type" defaultValue={deviceList[0]} style={{ minWidth: 275 }} onChange={handleDeviceChange}>
                        {deviceList.map(device => (
                            <Option key={device}>{device}</Option>
                        ))}
                    </Select>
                </Col>
            </Row>
        </div>
    )

}

export default DeviceTypes;