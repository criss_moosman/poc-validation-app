import React, { useState } from "react";

import { withRouter } from 'react-router';

const BarcodeInputField = ( props ) => {
  const [ deviceProduct, setEnteredDevice ] = useState(null);

  // TODO Setup routing to handle alternate workflows
  const onSubmit = (e) => {
    e.preventDefault();
    props.history.push(`/device/${deviceProduct}`);
  }

  const onInputChange = (e) => {
    setEnteredDevice(e.target.value);
  }

  return (
    <form onSubmit={onSubmit}>
      <input placeholder="e.g. 7622300710613" className="textInput" required type="number" onChange={onInputChange}/>
      <button className="btn" type="submit">Find</button>
    </form>
  )
}

export default withRouter(BarcodeInputField);
