import React from 'react';
import GPSI_LOGO from '../../assets/images/gps_logo.png';
import './header.css';

const Header = () => (
  <header className="header">
     <img src={GPSI_LOGO} alt="logo"/>
    {/* <h1 className="header__title">GPSI Device Scanner</h1> */}
  </header>
);

export default Header;
