/* eslint-disable no-nested-ternary */
import React, { useEffect, useState } from 'react';
import { Button } from 'antd';
import Header from '../uiElements/Header';
import DeviceFeaturesScreen from './deviceFeatureScreen';
import DeviceValidation from './deviceValidationScreen';
import AddOns from './addOnsScreen';

const StartScreen = (props) => {
    const {
        loadValidation,
        validatedDeviceProperties
    } = props;
    const [isPopulated, setIsPopulated] = useState(false);
    const [isValidated, setIsValidated] = useState(false);
    const [installKey, setInstallKey] = useState('');
    const [serialNumber, setSerialNumber] = useState('');

    const handleValidation = () => {
        props.showModal(installKey,serialNumber);
    }
    
    const enableNextButton = (installKeyParam, serialNumberParam) => {
        setInstallKey(installKeyParam);
        setSerialNumber(serialNumberParam)
        setIsPopulated(!isPopulated);
    }

    const handleDone = (event) => {
        window.scrollTo(0, 0);
        setIsValidated(false);
        setIsPopulated(false);
        setSerialNumber('');
        setInstallKey('');
        props.onCloseAlerts(event);
    }

    useEffect(() => {
        async function getParamsFromFields() {
            setIsValidated(loadValidation);
        }
        getParamsFromFields();
    }, [loadValidation]);

    return(
        <div className="start-screen-wrapper">
            <Header />
            {!isValidated ?
                <>
                    <DeviceFeaturesScreen 
                        enableNextButton={enableNextButton}/>
                    <AddOns />
                </>
            :
                <DeviceValidation validatedDeviceProperties={validatedDeviceProperties}/>
            }
            <div className="cta-wrapper">
                {isPopulated ?
                    isValidated ?
                        <Button type="primary" onClick={handleDone}>
                            Rescan                  
                        </Button>    
                        : 
                        <Button type="primary" onClick={handleValidation}>
                            Next                    
                        </Button>
                : 
                
                    <Button type="primary" onClick={handleValidation} disabled>
                        {isValidated ? "Next" : "Done" }
                    </Button>
                }
            </div>
        </div>
    )

}

export default StartScreen;