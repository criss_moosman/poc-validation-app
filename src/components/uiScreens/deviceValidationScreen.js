/* eslint-disable no-console */
import React, { useEffect, useState } from 'react';
import { Row, Col, Typography } from 'antd';

const DeviceValidation = (props) => {
    const {
        validatedDeviceProperties
    } = props;
    const [serialNumber, setSerialNumber] = useState('');
    const [vehicleLabel, setVehicleLabel] = useState('');
    const [vin, setVin] = useState('');
    const [odometer, setOdometer] = useState('');
    const [vehicleRunTime, setVehicleRunTime] = useState('');
    const [country, setCountry] = useState('');
    const [licenseState, setLicenseState] = useState('');
    const [licensePlate, setLicensePlate] = useState('');
    const [color, setColor] = useState('');

    const { Title } = Typography;

    useEffect(() => {
        async function getParamsFromFields() {
            console.log(validatedDeviceProperties[0].serial_number);
            setSerialNumber(validatedDeviceProperties[0].serial_number);
            setVehicleLabel(validatedDeviceProperties[0].label);
            setVin(validatedDeviceProperties[0].vin);
            setOdometer(validatedDeviceProperties[0].odometer);
            setVehicleRunTime('240');
            setCountry(validatedDeviceProperties[0].country);
            setLicenseState(validatedDeviceProperties[0].license_state);
            setLicensePlate('CA-COM456');
            setColor(validatedDeviceProperties[0].color);
        }
        getParamsFromFields();
    }, [
        serialNumber, 
        vehicleLabel,
        vin,
        odometer,
        vehicleRunTime,
        country,
        licenseState,
        licensePlate,
        color,
        validatedDeviceProperties,
    ]);

   
    return(
        <div className="device-features-wrapper">
            <Row>
                <Col span={12}>
                    <Title level={5}>Serial Number*</Title>
                </Col>
                <Col span={6}>
                <Title level={5}>{serialNumber}</Title>
                </Col>
            </Row>
            <Row>
                <Col span={12}>
                    <Title level={5}>Vehicle Label*</Title>
                </Col>
                <Col span={6}>
                    <Title level={5}>{vehicleLabel}</Title>
                </Col>
            </Row>
            <Row>
                <Col span={12}>
                    <Title level={5}>VIN*</Title>
                </Col>
                <Col span={6}>
                    <Title level={5}>{vin}</Title>
                </Col>
            </Row>
            <Row>
                <Col span={12}>
                    <Title level={5}>Odometer*</Title>
                </Col>
                <Col span={6}>
                    <Title level={5}>{odometer}</Title>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Title level={5}>Vehicle Run Time(hrs)*</Title>
                </Col>
                <Col span={6}>
                    <Title level={5}>{vehicleRunTime}</Title>
                </Col>
            </Row>
            <Row>
                <Col span={12}>
                    <Title level={5}>Country*</Title>
                </Col>
                <Col span={6}>
                    <Title level={5}>{country}</Title>
                </Col>
            </Row>
            <Row>
                <Col span={12}>
                    <Title level={5}>License State*</Title>
                </Col>
                <Col span={6}>
                    <Title level={5}>{licenseState}</Title>
                </Col>
            </Row>
            <Row>
                <Col span={12}>
                    <Title level={5}>License Plate*</Title>
                </Col>
                <Col span={6}>
                    <Title level={5}>{licensePlate}</Title>
                </Col>
            </Row>
            <Row>
                <Col span={12}>
                    <Title level={5}>Color</Title>
                </Col>
                <Col span={6}>
                    <Title level={5}>{color}</Title>
                </Col>
            </Row>
        </div>
    )

}

export default DeviceValidation;