/* eslint-disable no-console */
import React from 'react';
import { Row, Col, Typography, Checkbox } from 'antd';

const AddOns = () => {
    const { Title } = Typography;

    function onChange(e) {
        console.log(`checked = ${e.target.checked}`);
    }

    return(
        <div className="add-ons-wrapper">
            <Row>
                <Col style={{marginRight: '10px'}}>
                    <Checkbox onChange={onChange}/>
                </Col>
                <Col>
                    <Title level={5}>Input(s)</Title>
                </Col>
            </Row>
            <Row>
                <Col style={{marginRight: '10px'}}>
                    <Checkbox onChange={onChange}/>
                </Col>
                <Col>
                    <Title level={5}>Driver ID</Title>
                </Col>
            </Row>
        </div>
    )

}

export default AddOns;