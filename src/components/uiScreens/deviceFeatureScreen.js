import React, { useEffect, useState } from 'react';
import { Row, Col, Input, Typography } from 'antd';
import DeviceType from '../uiElements/deviceType';
import Video from '../utils/Video';


const DeviceFeatures = (props) => {
    const [installKey, setInstallKey] = useState('');
    const [serialNumber, setSerialNumber] = useState('');
    const [deviceType, setDeviceType] = useState('');
    const [showScanner, setShowScanner] = useState(false);

    const { Title } = Typography;

    const handleInstallKey = (e) => {
        setInstallKey(e.target.value);
    }

    const handleDeviceTypeSwitch = (value) => {
        setDeviceType(value);
    }

    // TODO Remove the hard coded mapping of install key 100914 this was for demo purposes only
    const handleLoadScanner = (results) => {
        if(installKey === '100914' && typeof results === 'string') {
            setSerialNumber('4871077091');
        } else if (installKey !== '100914' && typeof results === 'string'){
            setSerialNumber(results);
        }
        setShowScanner(!showScanner); 
    }

    useEffect(() => {
        async function getParamsFromFields() {
            setInstallKey(installKey);
            setSerialNumber(serialNumber);
            setDeviceType(deviceType);
        }
        getParamsFromFields();
    }, [installKey, serialNumber, deviceType]);

    const enableNextButton = () => {
        props.enableNextButton(installKey, serialNumber);
    }

    return(
        <div className="device-features-wrapper">
            <Row>
                <Col span={12}>
                    <Title level={5}>Install Key*</Title>
                </Col>
                <Col span={6}>
                    <Input className="install-key" placeholder="Install Key" defaultValue={installKey} onChange={handleInstallKey}/>
                </Col>
            </Row>
            <Row>
                <Col span={12}>
                    <Title level={5}>Serial Number*</Title>
                </Col>
                <Col span={6}>
                    <Input className="serial-number" placeholder="Serial Number" defaultValue={serialNumber} value={serialNumber} onClick={handleLoadScanner}/>
                </Col>
            </Row>
            <Row>
                <Col span={12}>
                    <Title level={5}>Device Type*</Title>
                </Col>
                <Col span={6}>
                    <DeviceType 
                        handleDeviceTypeSwitch={handleDeviceTypeSwitch}
                        enableNextButton={enableNextButton}    
                    />
                </Col>
            </Row>
            {showScanner && 
                <Video handleLoadScanner={handleLoadScanner}/>
            }
        </div>
    )

}

export default DeviceFeatures;