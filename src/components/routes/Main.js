import React from 'react';
import { Switch, Route } from 'react-router-dom';

import CameraHandler from '../cameraHandler';

// TODO setup routing to handle alternate workfolows
const Main = () => (
  <main className="main__wrapper">
    <Switch>
      <Route exact path='/' component={CameraHandler}/>
    </Switch>
  </main>
);

export default Main;
