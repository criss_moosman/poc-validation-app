/* eslint-disable dot-notation */
/* eslint-disable no-console */
/* eslint-disable func-names */
import React, { useCallback, useEffect, useState } from 'react';
import { Alert, Button, Layout, Modal, Spin } from 'antd';
import axios from 'axios';
import StartScreen from './uiScreens/startScreen';

function App() {
  const { Header, Footer, Content } = Layout;
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isIgnitionOn, setIsIgnitionOn] = useState(true);
  const [loadValidation, setLoadValidation] = useState(false);
  const [installKey, setInstallKey] = useState('');
  const [serialNumber, setSerialNumber] = useState('');
  // alert messaging state control
  const [isError, setIsError] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [errorCode, setErrorCode] = useState('');
  const [isVehicleInstalled, setIsVehicleInstalled] = useState(false);
  const [applicationHieght, setApplicationHieght] = useState(568);

  // load validated device props
  const [validatedDeviceProperties, setValidatedDeviceProperties] = useState();

  // TODO move all api calls to api.js file 
  const decodeDevice = useCallback((token, vin) => {
    axios.get(`https://api.gpsinsight.com/v2/vin/decode?vin=${vin}&session_token=${token}`)
    .then(function(response){
      setIsLoading(false);
      setIsModalVisible(false);
      setLoadValidation(true);
      setIsVehicleInstalled(true);
      const deviceData = response.data;
      console.log('Device Data: ', deviceData); 
    })
    .catch(function(error){
        setIsError(true);
        setIsModalVisible(false);
        setErrorMessage(error);
        console.log(error);
    });
  }, []);

  const validateDevice = useCallback((token) => {
    axios.get(`https://api.gpsinsight.com/v2//vehicle/list?session_token=${token}&vehicle=${serialNumber}&detail=1`)
    .then(function(response){
      const vehicle = response.data.data.filter( (vinNumber) => vinNumber.serial_number.toString().includes(serialNumber));
      const {vin} = vehicle[0];
      setValidatedDeviceProperties(vehicle);
      console.log('Validate Data: ', response.data); 
      decodeDevice(token, vin);
      console.log('Vin Data: ', vin); 
    })
    .catch(function(error){
        setIsError(true);
        setIsModalVisible(false);
        setErrorMessage(error);
        console.log(error);
    });
  }, [decodeDevice, serialNumber]);

  const systemAuth = useCallback(() => {
    axios.get(`https://api.gpsinsight.com/v2/userauth/login?user_context=admin&serial_number=${serialNumber}&install_key=${installKey}`)
    .then(function(response){
      const authToken = response.data;
      console.log('Auth Data: ', authToken);  
      validateDevice(authToken.data.token);
    })
    .catch(function(error){
        setIsError(true);
        setIsModalVisible(false);
        const errorResponseCode = error.response.data.errors[0].code;
        setErrorCode(errorResponseCode);
        const errorResponseMessage = error.response.data.errors[0].error;
        setErrorMessage(errorResponseMessage);
        console.log(error);
    });
  }, [validateDevice,installKey, serialNumber]);

  // eslint-disable-next-line no-shadow
  const showModal = (installKey, serialNumber) => {
    setInstallKey(installKey);
    setSerialNumber(serialNumber);
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handlevalidationEvent = () => {
    setIsLoading(true);
    systemAuth();
  }

  const onCloseAlerts = (e) => {
    console.log(e, 'I was closed.');
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    const appHieght = window.innerHeight;
    setApplicationHieght(appHieght);
  }, []);

  return (
    <div className="App">
       <Layout style={{minHeight: applicationHieght}}>
          <Header>
            <a 
              href="https://help.gpsinsight.com/installation-guides/">Install Guides</a>
          </Header>
          <Content>
            {isError && 
              <Alert
                message={`Error ${errorCode}`}
                description={errorMessage}
                type="error"
                closable
                onClose={onCloseAlerts}
              />
            }
            {isVehicleInstalled &&
              <Alert
                message="Already Registered"
                description="Do you want to transfer the device instead?"
                type="info"
                closable
                onClose={onCloseAlerts}
              />
            }
            <StartScreen 
              showModal={showModal} 
              onCloseAlerts={onCloseAlerts}
              loadValidation={loadValidation}
              validatedDeviceProperties={validatedDeviceProperties}
            />
          </Content>
          <Footer>© 2021 GPS Insight</Footer>
        </Layout>
        <Modal 
          title="Validate Device" 
          visible={isModalVisible} 
          footer={null}
          onCancel={handleCancel}>
              <Button 
                block 
                style={{marginBottom: '10px'}}
                disabled
              >
                Turn Ignition On
              </Button>
            {isIgnitionOn ?
              <Button 
                type="primary"
                onClick={handlevalidationEvent}
              >
                Verify Now
              </Button>
            :
              <Button 
                type="primary"
                disabled
              >
                Verify Now
              </Button> 
            }
            {isLoading &&
              <div className="load-spinner">
                <Spin />
              </div>
            }
        </Modal>
    </div>
  );
}

export default App;
