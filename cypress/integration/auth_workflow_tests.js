import { Decoder } from '@nuintun/qrcode';
const qrcode = new Decoder();
const Image = 'cypress/snapshots/actual/Auth/qr-auth-reader.js/qrcode.png';

describe('Functional test of the app', () => {
  it('is working', () => {
    expect(true).to.equal(true)
  })

  it('Should load validation app', () => {
    cy.visit("/");
  });
});

describe('Validator App Workflow', () => {
  beforeEach(() => {
    cy.visit('/');
  })

  it('it set form field values and calls auth api', () => {
      cy.get('input.install-key')
      .type("100914")

      cy.get('input.serial-number')
      .type("4871077091")
      .click({ force: true })

      cy.get('svg').screenshot('qrcode', {
        onAfterScreenshot($el, Image) {
            console.log('========Test1======', Image)
            qrcode
                .scan(Image)
                .then(result => {
                    console.log('=====Test2=====', result);
                })
        }
      })

      cy.get('#device-type')
      .click({ force: true })
      .then(() => {
        cy.contains('gpsi4000_vpod').click()
      })

      cy.contains('Next')      
      .click() 

      cy.contains('Verify Now')      
      .click() 

      cy.request('https://api.gpsinsight.com/v2/userauth/login?user_context=admin&serial_number=4871077091&install_key=100914')
      .should((response) => {
        expect(response.status).to.eq(200)
      })
      
  })
})